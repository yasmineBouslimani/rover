import { createApp } from 'vue'
import App from './App.vue'
import '@/ui/assets/tailwind.css'
createApp(App).mount('#app')
